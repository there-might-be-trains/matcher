#! /usr/bin/env python3

"""
This small script constructs a valid gradle command line,
while taking care of all the random escapes
"""

import subprocess as sp
import shlex
from sys import argv

GRADLE_CMD_LINE = ["./gradlew", "run"]

def main(base, params, argstr=""):
	paramstr = argstr + shlex.join(params)
	parameter_list = base + [paramstr]
	print("executing", parameter_list)
	sp.run(parameter_list, shell=False)

if __name__ == '__main__':
	main(GRADLE_CMD_LINE, argv[1:], "--args=")