package net.tmbt.matcher.geometric

import org.testng.Assert.*
import org.testng.annotations.Test
import kotlin.math.max
import kotlin.math.sqrt
import kotlin.random.Random

internal class QuadtreeTest {
    @Test
    fun testCreation() {
        val a = Point2D(2, 2)
        val b = Point2D(7, 5)
        val c = Point2D(5, 5)
        val d = Point2D(6, 3)
        val e = Point2D(10, 10)
        val f = Point2D(3, 9)
        val g = Point2D(5, 8.5)
        val h = Point2D(3.5, 7)
        val i = Point2D(5.5, 6.5)
        val j = Point2D(8.5, 9.5)
        val k = Point2D(9,9)
        val l = Point2D(9.5, 8.5)

        val points = listOf(a, b, c, d, e, f, g, h, i, j, k, l)
        val labels = "a b c d e f g h i j k l".split(" ")
        val data = points.zip(labels)

        val q = Quadtree(2, data, 1)
        assertEquals(q.items().toSet(), data.toSet())
        assertEquals(q.nearest(Point2D(6.5, 5)), data[1])
        assertEquals(q.multiple(Point2D(6.5, 5), 3), listOf(1, 2, 8).map {data[it]})
        assertEquals(q.multiple(Point2D(6.5, 5), 10, 2.1), listOf(1, 2, 8, 3).map {data[it]})

        assertNull(q.nearest(Point2D(10, 0), 2.0))
    }

    @Test
    fun testLargerCells() {
        val a = Point2D(2, 2)
        val b = Point2D(7, 5)
        val c = Point2D(5, 5)
        val d = Point2D(6, 3)
        val e = Point2D(10, 10)
        val f = Point2D(3, 9)
        val g = Point2D(5, 8.5)
        val h = Point2D(3.5, 7)
        val i = Point2D(5.5, 6.5)
        val j = Point2D(8.5, 9.5)
        val k = Point2D(9,9)
        val l = Point2D(9.5, 8.5)

        val points = listOf(a, b, c, d, e, f, g, h, i, j, k, l)
        val labels = "a b c d e f g h i j k l".split(" ")
        val data = points.zip(labels)

        val q = Quadtree(2, data, 4)
        assertEquals(q.items().toSet(), data.toSet())
        assertEquals(q.nearest(Point2D(6.5, 5)), data[1])
        assertEquals(q.multiple(Point2D(6.5, 5), 3), listOf(1, 2, 8).map {data[it]})
        assertEquals(q.multiple(Point2D(6.5, 5), 10, 2.1), listOf(1, 2, 8, 3).map {data[it]})

        assertNull(q.nearest(Point2D(10, 0), 2.0))
    }

    private fun <PointT : Point<PointT, VectorT>, VectorT : Vector<VectorT>, Data> validate(
        query: PointT,
        count: Int,
        radius: Double,
        result: List<Pair<PointT, Data>>,
        data: List<Pair<PointT, Data>>
    ) {
        val result = result.toSet()
        assertTrue(result.size <= count)
        for (it in result) {
            assertTrue(query.distance(it.first) < radius)
        }

        val farthest = result.fold(0.0, { acc, (p, _) -> max(acc, query.squareDistance(p)) })

        for (it in data) {
            if (query.squareDistance(it.first) < farthest) {
                assertTrue(result.contains(it))
            }
        }
    }

    @Test
    fun closePoints() {
        val p = Point2D()
        val q = Quadtree(2, listOf(p, p, p).zip((1..3)), 1)
        // If we got here without stack overflows everything is fine
        assertEquals(q.items().size, 3)
    }

    @Test
    fun bigFuzzyTest() {
        val rand = Random(4711)
        for (i in (1 .. 10)) {
            val COUNT = 10000
            val QUERIES = 100
            val MIN_X = -100.0
            val MAX_X = 100.0
            val MIN_Y = -123.0
            val MAX_Y = 123.0

            val data =
                (0 until COUNT).map { Pair(Point2D(rand.nextDouble(MIN_X, MAX_X), rand.nextDouble(MIN_Y, MAX_Y)), it) }

            val q = Quadtree(2, data, 4)
            assertEquals(data.toSet(), q.items().toSet())

            for (i in (0 until QUERIES)) {
                val center = Point2D(rand.nextDouble(MIN_X, MAX_X), rand.nextDouble(MIN_Y, MAX_Y))
                val count = rand.nextInt(20)
                val radius = rand.nextDouble(max(MAX_X - MIN_X, MAX_Y - MIN_Y))
                val result = q.multiple(center, count, radius)
                validate(center, count, radius, result, data)
            }
        }
    }

    @Test
    fun bigFuzzyTest3D() {
        val rand = Random(4711)
        for (i in (1..10)) {
            val COUNT = 10000
            val QUERIES = 100
            val MIN_X = -100.0
            val MAX_X = 100.0
            val MIN_Y = -123.0
            val MAX_Y = 123.0
            val MIN_Z = -456.0
            val MAX_Z = 456.0


            val data =
                (0 until COUNT).map {
                    Pair(
                        Point3D(
                            rand.nextDouble(MIN_X, MAX_X),
                            rand.nextDouble(MIN_Y, MAX_Y),
                            rand.nextDouble(MIN_Z, MAX_Z)
                        ), it
                    )
                }

            val q = Quadtree(3, data, 4)
            assertEquals(data.toSet(), q.items().toSet())

            for (i in (0 until QUERIES)) {
                val center = Point3D(
                    rand.nextDouble(MIN_X, MAX_X),
                    rand.nextDouble(MIN_Y, MAX_Y),
                    rand.nextDouble(MIN_Z, MAX_Z)
                )
                val count = rand.nextInt(20)
                val radius = rand.nextDouble(max(MAX_X - MIN_X, MAX_Y - MIN_Y))
                val result = q.multiple(center, count, radius)
                validate(center, count, radius, result, data)
            }
        }
    }
}