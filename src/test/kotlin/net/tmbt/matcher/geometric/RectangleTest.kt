package net.tmbt.matcher.geometric

import org.testng.Assert.*
import org.testng.annotations.Test
import kotlin.math.sqrt

internal class RectangleTest {
    companion object {
        const val DELTA = 1E-8
    }
    @Test
    fun testRect() {
        val r = CartesianRectangle<Point2D, Vector2D>(Point2D(40,0), Point2D(10,20))
        assertEquals(r.minCorner, Point2D(10, 0))
        assertEquals(r.maxCorner, Point2D(40, 20))
        assertEquals(r.center, Point2D(25, 10))
        assertEquals(r.dimensions, 2)
    }

    @Test
    fun testDistance() {
        val r = CartesianRectangle<Point2D, Vector2D>(Point2D(10, 10), Point2D(20, 20))
        assertEquals(r.distance(Point2D(0, 15)), 10.0, DELTA)
        assertEquals(r.squareDistance(Point2D(0, 15)), 100.0, DELTA)
        assertEquals(r.distance(Point2D(15, 15)), 0.0, DELTA)

        for (p in listOf(Point2D(0,0), Point2D(0,30), Point2D(30, 0), Point2D(30, 30))) {
            assertEquals(r.distance(p), sqrt(200.0), DELTA)
            assertEquals(r.squareDistance(p), 200.0, DELTA)
        }

        for (i in (0 until 4)) {
            assertEquals(r.distance(r[i]), 0.0, DELTA)
            assertEquals(r.squareDistance(r[i]), 0.0, DELTA)
        }

        assertEquals(r.distance(r.center), 0.0, DELTA)
        assertEquals(r.distance(Point2D(12, 12)), 0.0, DELTA)
    }

    @Test
    fun testCorners() {
        val r = CartesianRectangle<Point2D, Vector2D>(Point2D(40,0), Point2D(10,20))
        assertEquals(r[0], r.minCorner)
        assertEquals(r[3], r.maxCorner)
        assertEquals(r[1], Point2D(10, 20))
        assertEquals(r[2], Point2D(40, 0))
        for (i in (0 until 4)) {
            assertEquals(r.quadrant(r[i]), i)
        }
    }

}