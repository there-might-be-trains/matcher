package net.tmbt.matcher.geometric

import org.testng.Assert
import org.testng.annotations.Test

internal class PointTest {
    @Test
    fun pointAccessors() {
        val v2d = Point2D(Vector2D(listOf(0.0, 1.0)))
        Assert.assertEquals(v2d.x, 0.0)
        Assert.assertEquals(v2d[0], 0.0)
        Assert.assertEquals(v2d.y, 1.0)
        Assert.assertEquals(v2d[1], 1.0)

        val empty2d = v2d.empty()
        Assert.assertEquals(empty2d.x, 0.0)
        Assert.assertEquals(empty2d[0], 0.0)
        Assert.assertEquals(empty2d.y, 0.0)
        Assert.assertEquals(empty2d[1], 0.0)

        val v3d = Point3D(Vector3D(listOf(0.0, 1.0, 2.0)))
        Assert.assertEquals(v3d.x, 0.0)
        Assert.assertEquals(v3d[0], 0.0)
        Assert.assertEquals(v3d.y, 1.0)
        Assert.assertEquals(v3d[1], 1.0)
        Assert.assertEquals(v3d.z, 2.0)
        Assert.assertEquals(v3d[2], 2.0)

        val empty3d = v3d.empty()
        Assert.assertEquals(empty3d.x, 0.0)
        Assert.assertEquals(empty3d[0], 0.0)
        Assert.assertEquals(empty3d.y, 0.0)
        Assert.assertEquals(empty3d[1], 0.0)
        Assert.assertEquals(empty3d.z, 0.0)
        Assert.assertEquals(empty3d[2], 0.0)
    }

    @Test
    fun pointComparison() {
        val v3d = Point3D(listOf(0.0, 0.1, 0.2))
        Assert.assertEquals(v3d, Point3D(listOf(0.0, 0.1, 0.2)))
        Assert.assertEquals(v3d.empty(), Point3D(listOf(0.0, 0.0, 0.0)))
        Assert.assertEquals(Point3D(), Point3D(listOf(0.0, 0.0, 0.0)))
        Assert.assertNotEquals(v3d, v3d.empty())

        val v2d = Point2D(listOf(0.0, 0.1))
        Assert.assertEquals(v2d, Point2D(listOf(0.0, 0.1)))
        Assert.assertEquals(v2d.empty(), Point2D(listOf(0.0, 0.0)))
        Assert.assertEquals(Point2D(), Point2D(listOf(0.0, 0.0)))
        Assert.assertNotEquals(v2d, v2d.empty())

        val downcasted2d: Point<Point2D, Vector2D> = Point2D(0, 1)
        val normal2d = Point2D(0, 2)
        Assert.assertEquals(downcasted2d.compareAlong(normal2d, 0), 0)
        Assert.assertTrue(downcasted2d.compareAlong(normal2d, 1) < 0)
        Assert.assertEquals(normal2d.compareAlong(downcasted2d, 0), 0)
        Assert.assertTrue(normal2d.compareAlong(downcasted2d, 1) > 0)

        val downcasted3d: Point<Point3D, Vector3D> = Point3D(0, 1, 2)
        val normal3d = Point3D(0, 2, 4)
        Assert.assertEquals(downcasted3d.compareAlong(normal3d, 0), 0)
        Assert.assertTrue(downcasted3d.compareAlong(normal3d, 1) < 0)
        Assert.assertTrue(downcasted3d.compareAlong(normal3d, 2) < 0)
        Assert.assertEquals(normal3d.compareAlong(downcasted3d, 0), 0)
        Assert.assertTrue(normal3d.compareAlong(downcasted3d, 1) > 0)
        Assert.assertTrue(normal3d.compareAlong(downcasted3d, 2) > 0)
    }
    @Test
    fun pointDimensions() {
        val empty3d = Point3D.empty()
        Assert.assertEquals(empty3d.dimensions(), 3)
        Assert.assertTrue(empty3d.checkDimensions(3))
        Assert.assertFalse(empty3d.checkDimensions(2))

        val empty2d = Point2D.empty()
        Assert.assertEquals(empty2d.dimensions(), 2)
        Assert.assertTrue(empty2d.checkDimensions(2))
        Assert.assertTrue(empty2d.checkDimensions(2))
    }

    @Test
    fun emptyVectorSanityChecks() {
        Assert.assertEquals(Point2D().distance(Point2D()), 0.0)
        Assert.assertEquals(Point2D().minimum(Point2D()), Point2D())
        Assert.assertEquals(Point3D().distance(Point3D()), 0.0)
        Assert.assertEquals(Point3D().minimum(Point3D()), Point3D())

        Assert.assertEquals(Point2D().empty(), Point2D())
        Assert.assertEquals(Point3D().empty(), Point3D())

        val p2: Point<Point2D, Vector2D> = Point2D()
        val p3: Point<Point3D, Vector3D> = Point3D()
        Assert.assertEquals(p2.compare(p2), -1)
        Assert.assertEquals(p3.compare(p3), -1)
        Assert.assertEquals(p2.empty(), Point2D())
        Assert.assertEquals(p3.empty(), Point3D())

        Assert.assertEquals(Point2D() + Vector2D(), Point2D())
        Assert.assertEquals(Vector2D() + Point2D(), Point2D())
        Assert.assertEquals(Point2D() - Point2D(), Vector2D())

        Assert.assertEquals(Point3D() + Vector3D(), Point3D())
        Assert.assertEquals(Vector3D() + Point3D(), Point3D())
        Assert.assertEquals(Point3D() - Point3D(), Vector3D())

        Assert.assertEquals((p2 + (p2 - p2) * 1.0), Point2D())
        Assert.assertEquals((p3 + (p3 - p3) * 1.0), Point3D())
    }

    @Test
    fun constructors() {
        Assert.assertEquals(Point2D(), Point2D(listOf(0.0, 0.0)))
        Assert.assertEquals(Point2D(0.1, 0.2), Point2D(listOf(0.1, 0.2)))
        Assert.assertEquals(Point2D(1, 2), Point2D(listOf(1.0, 2.0)))
        Assert.assertEquals(Point2D(0.1, 20L), Point2D(listOf(0.1, 20.0)))

        Assert.assertEquals(Point3D(), Point3D(listOf(0.0, 0.0, 0.0)))
        Assert.assertEquals(Point3D(0.1, 0.2, 0.3), Point3D(listOf(0.1, 0.2, 0.3)))
        Assert.assertEquals(Point3D(1, 2, 3), Point3D(listOf(1.0, 2.0, 3.0)))
        Assert.assertEquals(Point3D(0.1, 20L, 5.toShort()), Point3D(listOf(0.1, 20.0, 5.0)))
    }

    @Test
    fun pointMaths() {
        Assert.assertEquals(Point2D(1, 0) + Vector2D(0, 1), Point2D(1, 1))
        Assert.assertEquals(Vector2D(1, 1) + Point2D(), Point2D(1, 1))
        Assert.assertEquals(Point2D(1, 1) - Point2D(1, 0), Vector2D(0, 1))
        Assert.assertEquals(Point2D(1, 1) - Point2D(), Vector2D(1, 1))
        Assert.assertEquals(Point3D(1, 0, 0) + Vector3D(0, 1, 0), Point3D(1, 1, 0))
        Assert.assertEquals(Vector3D(1, 1, 0) + Point3D(), Point3D(1, 1, 0))
        Assert.assertEquals(Point3D(1, 1, 0) - Point3D(0, 1, 1), Vector3D(1, 0, -1))
        Assert.assertEquals(Point3D(1, 1, 0) - Point3D(), Vector3D(1, 1, 0))

        Assert.assertEquals(Point2D(1, 0).minimum(Point2D(0, 1)), Point2D(0, 0))
        Assert.assertEquals(Point2D(1, 0).maximum(Point2D(0, 1)), Point2D(1, 1))
        Assert.assertEquals(Point2D(1, 2).minimum(Point2D(0, 3)), Point2D(0, 2))
        Assert.assertEquals(Point2D(1, 2).maximum(Point2D(0, 3)), Point2D(1, 3))

        Assert.assertEquals(Point3D(1, 0, 0).minimum(Point3D(0, 1, 0)), Point3D(0, 0, 0))
        Assert.assertEquals(Point3D(1, 0, 0).maximum(Point3D(0, 1, 0)), Point3D(1, 1, 0))
        Assert.assertEquals(Point3D(1, 0, 2).minimum(Point3D(0, 1, 1)), Point3D(0, 0, 1))
        Assert.assertEquals(Point3D(1, 0, 2).maximum(Point3D(0, 1, 1)), Point3D(1, 1, 2))
    }

    @Test
    fun pointMisc() {
        val p0 = Point2D(-3, 5)
        val q0 = Point2D(7, 2)
        println((0 until 4).map( {p0.createCorner(it, q0)}))
        Assert.assertEquals(p0.createCorner(0, q0), Point2D(-3, 2))
        Assert.assertEquals(p0.createCorner(1, q0), Point2D(-3, 5))
        Assert.assertEquals(p0.createCorner(2, q0), Point2D(7, 2))
        Assert.assertEquals(p0.createCorner(3, q0), Point2D(7, 5))

        val p1 = Point2D(42, 150)
        val q1 = Point2D(123, 456)
        Assert.assertEquals(p1.createCorner(0, q1), Point2D(42, 150))
        Assert.assertEquals(p1.createCorner(1, q1), Point2D(42, 456))
        Assert.assertEquals(p1.createCorner(2, q1), Point2D(123, 150))
        Assert.assertEquals(p1.createCorner(3, q1), Point2D(123, 456))
    }

    @Test
    fun pointCompare() {
        val p = Point2D(1, 1)
        Assert.assertEquals(p.compare(Point2D(1,1)), -1)
        Assert.assertEquals(p.compare(Point2D(0,0)), 0)
        Assert.assertEquals(p.compare(Point2D(2,2)), 3)
        Assert.assertEquals(p.compare(Point2D(2,0)), 2)
        Assert.assertEquals(p.compare(Point2D(0,2)), 1)
        //TODO: special cases: same x, same y. Semantics?
    }

    @Test
    fun testInterpolate() {
        Assert.assertEquals(Point2D(0,0).interpolate(0.0, Point2D(3, 4)), Point2D(0,0))
        Assert.assertEquals(Point2D(0,0).interpolate(1.0, Point2D(3, 4)), Point2D(3,4))
        Assert.assertEquals(Point2D(0,0).interpolate(.5, Point2D(3, 4)), Point2D(1.5, 2))
        Assert.assertEquals(Point2D(0,0).interpolate(2.0, Point2D(3, 4)), Point2D(6,8))
        Assert.assertEquals(Point2D(0,0).interpolate(-1.0, Point2D(3, 4)), Point2D(-3, -4))
    }

    @Test
    fun pointCompareAndCorner() {
        val p = Point2D(-2, -2)
        val q = Point2D(6, 8)
        val m = Point2D(-1, 4)
        for (i in (0 until 4))
            Assert.assertEquals(m.compare(p.createCorner(i, q)), i)
    }
}