package net.tmbt.matcher.geometric

import org.testng.annotations.Test
import org.testng.Assert.*

internal class VectorTest {
    @Test
    fun vectorAccessors() {
        val v2d = Vector2D(listOf(0.0, 1.0))
        assertEquals(v2d.x, 0.0)
        assertEquals(v2d[0], 0.0)
        assertEquals(v2d.y, 1.0)
        assertEquals(v2d[1], 1.0)

        val empty2d = v2d.empty()
        assertEquals(empty2d.x, 0.0)
        assertEquals(empty2d[0], 0.0)
        assertEquals(empty2d.y, 0.0)
        assertEquals(empty2d[1], 0.0)

        val v3d = Vector3D(listOf(0.0, 1.0, 2.0))
        assertEquals(v3d.x, 0.0)
        assertEquals(v3d[0], 0.0)
        assertEquals(v3d.y, 1.0)
        assertEquals(v3d[1], 1.0)
        assertEquals(v3d.z, 2.0)
        assertEquals(v3d[2], 2.0)

        val empty3d = v3d.empty()
        assertEquals(empty3d.x, 0.0)
        assertEquals(empty3d[0], 0.0)
        assertEquals(empty3d.y, 0.0)
        assertEquals(empty3d[1], 0.0)
        assertEquals(empty3d.z, 0.0)
        assertEquals(empty3d[2], 0.0)
    }

    @Test
    fun vectorComparison() {
        val v3d = Vector3D(listOf(0.0, 0.1, 0.2))
        assertEquals(v3d, Vector3D(listOf(0.0, 0.1, 0.2)))
        assertEquals(v3d.empty(), Vector3D(listOf(0.0, 0.0, 0.0)))
        assertEquals(Vector3D(), Vector3D(listOf(0.0, 0.0, 0.0)))
        assertNotEquals(v3d, v3d.empty())

        val v2d = Vector2D(listOf(0.0, 0.1))
        assertEquals(v2d, Vector2D(listOf(0.0, 0.1)))
        assertEquals(v2d.empty(), Vector2D(listOf(0.0, 0.0)))
        assertEquals(Vector2D(), Vector2D(listOf(0.0, 0.0)))
        assertNotEquals(v2d, v2d.empty())

        val downcasted2d: Vector<Vector2D> = Vector2D(0, 1)
        val normal2d = Vector2D(0, 2)
        assertEquals(downcasted2d.compareAlong(normal2d, 0), 0)
        assertTrue(downcasted2d.compareAlong(normal2d, 1) < 0)
        assertEquals(normal2d.compareAlong(downcasted2d, 0), 0)
        assertTrue(normal2d.compareAlong(downcasted2d, 1) > 0)

        val downcasted3d: Vector<Vector3D> = Vector3D(0, 1, 2)
        val normal3d = Vector3D(0, 2, 4)
        assertEquals(downcasted3d.compareAlong(normal3d, 0), 0)
        assertTrue(downcasted3d.compareAlong(normal3d, 1) < 0)
        assertTrue(downcasted3d.compareAlong(normal3d, 2) < 0)
        assertEquals(normal3d.compareAlong(downcasted3d, 0), 0)
        assertTrue(normal3d.compareAlong(downcasted3d, 1) > 0)
        assertTrue(normal3d.compareAlong(downcasted3d, 2) > 0)
    }
    @Test
    fun vectorDimensions() {
        val empty3d = Vector3D.empty()
        assertEquals(empty3d.dimensions(), 3)
        assertTrue(empty3d.checkDimensions(3))
        assertFalse(empty3d.checkDimensions(2))

        val empty2d = Vector2D.empty()
        assertEquals(empty2d.dimensions(), 2)
        assertTrue(empty2d.checkDimensions(2))
        assertTrue(empty2d.checkDimensions(2))
    }

    @Test
    fun emptyVectorSanityChecks() {
        assertEquals(Vector2D().length(), 0.0)
        assertEquals(Vector2D().dot(Vector2D()), 0.0)
        assertEquals(Vector3D().length(), 0.0)
        assertEquals(Vector3D().dot(Vector3D()), 0.0)

        assertEquals(Vector2D().empty(), Vector2D())
        assertEquals(Vector3D().empty(), Vector3D())

        val v2: Vector<Vector2D> = Vector2D()
        val v3: Vector<Vector3D> = Vector3D()
        assertEquals(v2.dot(v2), v2.length())
        assertEquals(v3.dot(v3), v3.length())
        assertEquals(v2.empty(), Vector2D())
        assertEquals(v3.empty(), Vector3D())

        assertEquals(Vector2D() + Vector2D(), Vector2D())
        assertEquals(Vector2D() - Vector2D(), Vector2D())
        assertEquals(Vector2D() * 1.0, Vector2D())
        assertEquals(Vector2D() / 1.0, Vector2D())

        assertEquals(Vector3D() + Vector3D(), Vector3D())
        assertEquals(Vector3D() - Vector3D(), Vector3D())
        assertEquals(Vector3D() * 1.0, Vector3D())
        assertEquals(Vector3D() / 1.0, Vector3D())

        assertEquals((v2 + v2 - v2 * 1.0 + v2 / 1.0), Vector2D())
        assertEquals((v3 + v3 - v3 * 1.0 + v3 / 1.0), Vector3D())
    }

    @Test
    fun constructors() {
        assertEquals(Vector2D(), Vector2D(listOf(0.0, 0.0)))
        assertEquals(Vector2D(0.1, 0.2), Vector2D(listOf(0.1, 0.2)))
        assertEquals(Vector2D(1, 2), Vector2D(listOf(1.0, 2.0)))
        assertEquals(Vector2D(0.1, 20L), Vector2D(listOf(0.1, 20.0)))

        assertEquals(Vector3D(), Vector3D(listOf(0.0, 0.0, 0.0)))
        assertEquals(Vector3D(0.1, 0.2, 0.3), Vector3D(listOf(0.1, 0.2, 0.3)))
        assertEquals(Vector3D(1, 2, 3), Vector3D(listOf(1.0, 2.0, 3.0)))
        assertEquals(Vector3D(0.1, 20L, 5.toShort()), Vector3D(listOf(0.1, 20.0, 5.0)))
    }

    @Test
    fun vectorMaths() {
        assertEquals(Vector2D(1, 0) + Vector2D(0, 1), Vector2D(1, 1))
        assertEquals(Vector2D(1, 1) + Vector2D(), Vector2D(1, 1))
        assertEquals(Vector2D(1, 1) - Vector2D(1, 0), Vector2D(0, 1))
        assertEquals(Vector2D(1, 1) - Vector2D(), Vector2D(1, 1))
        assertEquals(Vector3D(1, 0, 0) + Vector3D(0, 1, 0), Vector3D(1, 1, 0))
        assertEquals(Vector3D(1, 1, 0) + Vector3D(), Vector3D(1, 1, 0))
        assertEquals(Vector3D(1, 1, 0) - Vector3D(0, 1, 1), Vector3D(1, 0, -1))
        assertEquals(Vector3D(1, 1, 0)- Vector3D(), Vector3D(1, 1, 0))

        assertEquals(Vector3D(1, 0, 3) * 2, Vector3D(2, 0, 6))
        assertEquals(Vector3D(1, 0, 1) * 1.0, Vector3D(1, 0, 1))
        assertEquals(Vector3D(1, 1, 1) * 0L, Vector3D())
        assertEquals(2 * Vector3D(1, 0, 3), Vector3D(2, 0, 6))
        assertEquals(1.0 * Vector3D(1, 0, 1), Vector3D(1, 0, 1))
        assertEquals(0L * Vector3D(1, 1, 1), Vector3D())
        assertEquals(Vector3D(1, 0, 3) / 2, Vector3D(0.5, 0, 1.5))
        assertEquals(Vector3D(1, 0, 1) / 1.0, Vector3D(1, 0, 1))
        assertEquals(Vector3D(1, 1, 1) / 1L, Vector3D(1, 1, 1))

        assertEquals(Vector2D(1, 0) * 2, Vector2D(2, 0))
        assertEquals(Vector2D(1, 0) * 1.0, Vector2D(1, 0))
        assertEquals(Vector2D(1, 1) * 0L, Vector2D())
        assertEquals(2 * Vector2D(1, 0), Vector2D(2, 0))
        assertEquals(1.0 * Vector2D(1, 0), Vector2D(1, 0))
        assertEquals(0L * Vector2D(1, 1), Vector2D())
        assertEquals(Vector2D(1, 0) / 2, Vector2D(0.5, 0))
        assertEquals(Vector2D(1, 0) / 1.0, Vector2D(1, 0))
        assertEquals(Vector2D(1, 1) / 1L, Vector2D(1, 1))
    }
}