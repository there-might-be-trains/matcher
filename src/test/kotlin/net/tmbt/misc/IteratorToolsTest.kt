package net.tmbt.misc

import org.testng.Assert.*
import org.testng.annotations.Test


internal class IteratorToolsTest {
    @Test
    fun testStatefulIteration() {
        val l = listOf(1, 2, 3, 4, 5, 6)
        val it = makeStateful(l.iterator())
        println(makeStateful(l.iterator()).asSequence().toList())
        for (i in (1..5)) {
            assertEquals(it.head(), i)
            assertEquals(it.next(), i)
            assertTrue(it.hasNext())
        }
        assertEquals(it.next(), 6)
        assertEquals(it.head(), 6)
        assertFalse(it.hasNext())
    }

    @Test
    fun testMerge() {
        val l1 = listOf(1, 3, 5, 7, 9)
        val l2 = listOf(2, 4, 6, 8)
        println(mergeBy(l1, l2, {it}))
        assertEquals(merge(l1, l2), (1 .. 9).toList())
    }

    @Test
    fun testLimitedMerge() {
        val l1 = listOf(1, 3, 5, 7, 9)
        val l2 = listOf(2, 4, 6, 8)
        assertEquals(merge(l1, l2, 4), (1 .. 4).toList())
    }

    @Test
    fun testCompMerge() {
        val l1 = "alle meine Entchen".split(" ")
        val l2 = "schwimmen auf dem See".split(" ").sortedBy { it.length }

        assertEquals(mergeBy(l1, l2, {it.length}), "auf dem See alle meine Entchen schwimmen".split(" "))
    }
}

