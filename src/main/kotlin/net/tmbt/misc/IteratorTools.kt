package net.tmbt.misc

/**
 * An iterator that knows its current state.
 * Provides the [head] method which returns the current value without moving the cursor.
 */
interface StatefulIterator <T> : Iterator<T> {
    /**
     * Returns the value under the cursor.
     * This is the value that would be returned by calling [next] without moving the cursor.
     * @return the current value
     */
    fun head() : T
}

/**
 * Makes an iterator stateful.
 * Such an iterator provides access to the current value without moving the cursor.
 * @return a stateful iterator based on the specified iterator.
 * @see StatefulIterator
 */
fun <T>makeStateful(it: Iterator<T>) : StatefulIterator<T> = StatefulIteratorImplementation(it)

/**
 * Merges two sorted ranges to a new sorted range.
 * The values returned by the iterator will also be sorted.
 * @param it1 iterator of first sorted range
 * @param second iterator of second sorted range
 * @param comp comparator
 * @param limit maximum length of the resulting range
 * @return iterator of sorted merged range.
 */
fun <T, R : Comparable<R>> mergeBy(
    it1: Iterator<T>,
    it2: Iterator<T>,
    comp: (T) -> R,
    limit: Int = Int.MAX_VALUE
): StatefulIterator<T> = MergeIterator(it1, it2, comp, limit)

/**
 * Merges two sorted lists into a new sorted list.
 * In case of a tie the element in the first list will be chosen.
 * The resulting list can optionally be limited to a certain length.
 * @param first first sorted list
 * @param second second sorted list
 * @param comp comparator
 * @param limit maximum length of the resulting list
 * @return merged sorted list
 */
fun <T, R: Comparable<R>> mergeBy(
    first: List<T>,
    second: List<T>,
    comp: (T) -> R,
    limit: Int = Int.MAX_VALUE
): List<T> = mergeBy(first.iterator(), second.iterator(), comp, limit).asSequence().toMutableList()

/**
 * Merges two sorted lists into a new sorted list.
 * In case of a tie the element in the first list will be chosen.
 * The resulting list can optionally be limited to a certain length.
 * @param first first sorted list
 * @param second second sorted list
 * @param limit maximum length of the resulting list
 * @return merged sorted list
 */
fun <T: Comparable<T>> merge(
    first: List<T>,
    second: List<T>,
    limit: Int = Int.MAX_VALUE
) : List<T> = mergeBy(first, second, {it}, limit)

internal class StatefulIteratorImplementation <T> (it: Iterator<T>) : StatefulIterator<T> {
    private val base = it
    private var hadNext = false
    private var head : T? = null

    init {
        move()
    }

    private fun move() {
        hadNext = base.hasNext()
        if (hadNext) {
            head = base.next()
        }
    }

    override fun hasNext(): Boolean {
        return hadNext
    }

    override fun next(): T {
        var h = head
        move()
        return h!!
    }

    override fun head(): T {
        return head!!
    }
}

internal class MergeIterator<T, R : Comparable<R>>(
    first: Iterator<T>,
    second: Iterator<T>,
    val comp: (T) -> R,
    val limit: Int
) : StatefulIterator<T> {
    private var pos = 0
    private val first = makeStateful(first)
    private val second = makeStateful(second)

    override fun hasNext(): Boolean {
        return pos < limit && (first.hasNext() || second.hasNext())
    }

    override fun next(): T {
        ++pos
        if (!first.hasNext()) {
            return second.next()
        } else if (!second.hasNext())
            return first.next()
        else if (comp(first.head()) <= comp(second.head()))
            return first.next()
        else
            return second.next()
    }

    override fun head(): T {
        if (!first.hasNext())
            return second.head()
        else if (!second.hasNext())
            return first.head()
        else if (comp(first.head()) <= comp(second.head()))
            return first.head()
        else
            return second.head()
    }
}