package net.tmbt.matcher.geometric

import kotlin.math.max
import kotlin.math.min

/**
 * an abstract class that defines an (affine) point over a vector space VectorT
 * @param Inherited the points implementor
 * @param VectorT the underlying vector space
 */
interface Point<Inherited : Point<Inherited, VectorT>, VectorT : Vector<VectorT>> {
    /** make sure that the dimensions actually are matching */
    fun checkDimensions(dims: Int) = dims == dimensions()
    /** @return number of supported dimensions */
    fun dimensions(): Int

    /**
     * compare two points, mapping to a range of integers giving a spatial orientation
     * Example: 2d: [0 => upper left, 1 => upper right, 2 => lower left, 3 => lower right]
     * @return [0..2^dimensions()] for a direction, -1 for equal and -2 for not comparable
     */
    fun compare(point: Point<Inherited, VectorT>): Int

    /**
     * compare along an of [0..dimensions()] axis, useful for kd trees
     * @return -1 if smaller, 0 if equal (not same point, just same coordinate) or 1 if greater
     */
    fun compareAlong(point: Point<Inherited, VectorT>, axis: Int): Int

    /** @return the distance between two points */
    fun distance(other: Point<Inherited, VectorT>): Double = (this - other).length()

    /** @return the squared distance between the two points*/
    fun squareDistance(other: Point<Inherited, VectorT>) : Double = (this - other).squareLength()

    /**
     * @return distance to a separator in a multidimensional 3d structure
     * @attention the points need to be colinear in the subspace
     */
    //! \todo List inheritance?
    fun separatorDistance(points: List<Inherited>): Double

    /**
     * construct new corners
     * @param index which index as defined by compare() the corner shall have
     * @param other the other point
     * @return a new corner
     */
    fun createCorner(index: Int, other: Point<Inherited, VectorT>): Inherited

    /**
     * Interpolate linearly between two points.
     * Values of t \in [0, 1] result in a point on the line segment between the two points.
     * The following identities hold: `p.interpolate(0, q) == p` and `p.interpolate(1, q) == q`.
     * @param t position on the line between this point and the other point
     * @param other other point
     * @return interpolated point
     */
    fun interpolate(t: Double, other: Point<Inherited, VectorT>): Point<Inherited, VectorT>

    /** @return a point on the zero marker */
    fun empty(): Inherited
    /** @return new point = this + point */
    operator fun plus(v: Vector<VectorT>): Inherited
    /** @return new point = this - point */
    operator fun minus(point: Point<Inherited, VectorT>): VectorT

    /** @return pareto minimum of those two points */
    fun minimum(other: Point<Inherited, VectorT>): Inherited
    /** @return pareto maximum of those two points */
    fun maximum(other: Point<Inherited, VectorT>): Inherited

    /** if you need to do stuff that is hacky, convert to a vector */
    fun toVector(): VectorT
    /** if you've done something hacky, convert from a vector */
    fun fromVector(v: Vector<VectorT>): Inherited

    /**
     * if might be needed to type-safely downcast myself into my implementor
     * @return myself as an instance of Inherited
     */
    fun toInherited(): Inherited
}

/** commutativity is important */
operator fun <PointT: Point<PointT, VectorT>, VectorT : Vector<VectorT>> VectorT.plus(point: Point<PointT, VectorT>): PointT = point + this


/**
 * compare two points along an axis
 * @param PointT the point implementor
 */
class PointAxisComparator<PointT: Point<PointT, VectorT>, VectorT: Vector<VectorT>>(val index: Int) : Comparator<PointT> {
    override fun compare(fst: PointT, scn: PointT): Int = fst.compareAlong(scn, index)
}

/**
 * base class
 */
abstract class CartesianPoint<PointT: CartesianPoint<PointT, VectorT>, VectorT: EuclideanVector<VectorT>>
(initial: EuclideanVector<VectorT>, val dims: Int) : Point<PointT, VectorT> {
    open val underlying = initial
    init {
        require(initial.checkDimensions(dims)) {"the size does not match the dimensions of the point"}
    }
    val isIncomparable
        get() = underlying.data.any { it.isNaN() || it.isInfinite() }

    override fun dimensions(): Int = underlying.dimensions()

    //TODO: unclear semantics. Document quadrants somewhere
    override fun compare(point: Point<PointT, VectorT>): Int {
        if (this == point) return -1
        if (isIncomparable || point.toInherited().isIncomparable) return -2
        return quadrant(point.toInherited())
    }

    override fun compareAlong(point: Point<PointT, VectorT>, axis: Int) = underlying.compareAlong(point.toVector(), axis)

    override fun distance(other: Point<PointT, VectorT>): Double = (this - other).length()

    override fun createCorner(index: Int, other: Point<PointT, VectorT>): PointT {
        return create((0 until dims).map {
            if (index and (1 shl (dims - it - 1)) == 0)
                min(this[it], other.toInherited()[it])
            else
                max(this[it], other.toInherited()[it])
        }.toList())
    }

    override fun interpolate(t: Double, other: Point<PointT, VectorT>): Point<PointT, VectorT> {
        return this + t * (other - this)
    }

    override fun empty(): PointT = create(underlying.empty())
    override fun plus(v: Vector<VectorT>): PointT =
            create(underlying + v)
    override fun minus(point: Point<PointT, VectorT>): VectorT = underlying - point.toVector()

    override fun minimum(other: Point<PointT, VectorT>): PointT =
            create(createVector(underlying.data.zip(other.toVector().data){ fst, scn -> min(fst, scn) }))
    override fun maximum(other: Point<PointT, VectorT>): PointT =
            create(createVector(underlying.data.zip(other.toVector().data){ fst, scn -> max(fst, scn) }))

    abstract fun create(vector: EuclideanVector<VectorT>): PointT
    abstract fun create(data: List<Double>): PointT
    abstract fun createVector(vector: EuclideanVector<VectorT>): VectorT
    abstract fun createVector(data: List<Double>): VectorT

    /** actually access data */
    operator fun get(index: Int) = underlying[index]

    private fun quadrant(point: PointT): Int {
        var q = 0
        for (i in (0 until dims))
            if (point[i] > this[i])
                q += 1 shl (dims - i - 1)
        return q
    }

    override fun toVector(): VectorT = createVector(underlying)
    override fun fromVector(v: Vector<VectorT>): PointT = create(v.toInherited())
}

data class Point2D(override val underlying: EuclideanVector<Vector2D>)
    : CartesianPoint<Point2D, Vector2D>(underlying, DIMENSIONS) {
    companion object {
        /** this is a 2D-Point */
        const val DIMENSIONS = 2
        fun empty() = Point2D()
    }

    /**
     * create a point on the origin of our coordinate system
     */
    constructor() : this(Vector2D())

    /**
     * for convenience, use the data list directly
     * @param data list to be forwarded to the Vector
     */
    constructor(data: List<Double>) : this(Vector2D(data))

    /**
     * for convenience, directly offer a constructor taking the two coordinates
     * @param fst first coord
     * @param scn second coord
     */
    constructor(fst: Number, scn: Number) : this(Vector2D(fst, scn))

    val x
        get() = this[0]
    val y
        get() = this[1]

    override fun create(vector: EuclideanVector<Vector2D>): Point2D = Point2D(createVector(vector))
    override fun create(data: List<Double>): Point2D = Point2D(createVector(data))
    override fun createVector(vector: EuclideanVector<Vector2D>): Vector2D = Vector2D(vector.data)
    override fun createVector(data: List<Double>): Vector2D = Vector2D(data)

    override fun separatorDistance(points: List<Point2D>): Double {
        require(points.size == 2)
        return Line(points[0], points[1]).projectSegment(this).distance(this)
    }

    override fun toInherited() = this
}

data class Point3D(override val underlying: EuclideanVector<Vector3D>) : CartesianPoint<Point3D, Vector3D>(underlying, DIMENSIONS) {
    companion object {
        /** this is a 3D-Point */
        const val DIMENSIONS = 3
        fun empty() = Point3D()
    }

    /**
     * create a point on the origin of our coordinate system
     */
    constructor() : this(Vector3D())

    /**
     * for convenience, use the data list directly
     * @param data list to be forwarded to the Vector
     */
    constructor(data: List<Double>) : this(Vector3D(data))

    /**
     * for convenience, directly offer a constructor taking the two coordinates
     * @param fst first coord
     * @param scn second coord
     * @param thrd third coord
     */
    constructor(fst: Number, scn: Number, third: Number) : this(Vector3D(fst, scn, third))

    val x
        get() = this[0]
    val y
        get() = this[1]
    val z
        get() = this[2]

    override fun create(vector: EuclideanVector<Vector3D>): Point3D = Point3D(createVector(vector))
    override fun create(data: List<Double>): Point3D = Point3D(createVector(data))
    override fun createVector(vector: EuclideanVector<Vector3D>): Vector3D = Vector3D(vector.data)
    override fun createVector(data: List<Double>): Vector3D = Vector3D(data)

    override fun separatorDistance(points: List<Point3D>): Double {
        require(points.size >= 3)
        return Plane(points[0], points[1], points[2]).projectSegment(this).distance(this)
    }

    override fun toInherited() = this
}
