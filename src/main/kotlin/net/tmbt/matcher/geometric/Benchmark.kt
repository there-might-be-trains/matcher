package net.tmbt.matcher.geometric

import kotlin.random.Random
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val COUNT = 1000000
    val QUERIES = 100000
    val CAPACITY = 20

    println("initializing quadtree with $COUNT elements")
    val rand = Random(42)

    fun randPoint() : Point2D = Point2D(rand.nextDouble(1000.0), rand.nextDouble(1000.0))

    val data = (1 .. COUNT) .map {randPoint()} . zip((1 .. COUNT))

    var q0: Quadtree<Point2D, Vector2D, Int>? = null
    val timeInit = measureTimeMillis {
        q0 = Quadtree(2, data, CAPACITY)
    }
    println("Quadtree initialization took ${timeInit}ms")
    val q = q0!!

    val timeQueries = measureNanoTime {
        for (i in (1..QUERIES)) {
            val center = randPoint()
            q.nearest(center)
        }
    }
    println("$QUERIES queries took ${timeQueries / 1000 / 1000}ms (${timeQueries / QUERIES / 1e3}µs/query}")

    val timeNearest = measureNanoTime {
        for (i in (1..QUERIES)) {
            val center = randPoint()
            q.multiple(center, 20)
        }
    }
    println("$QUERIES queries took ${timeNearest / 1000 / 1000}ms (${timeNearest / QUERIES / 1e3}µs/query}")
}

