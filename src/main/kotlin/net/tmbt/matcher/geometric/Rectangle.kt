package net.tmbt.matcher.geometric

import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/**
 * A rectangular shape in a certain number of dimensions.
 */
interface Rectangle<Rect, PointT : Point<PointT, VectorT>, VectorT : Vector<VectorT>> {
    /**
     * Distance between this rectangle and [point].
     * @param point point
     * @return distance to [point]
     */
    fun distance(point: PointT) : Double = sqrt(squareDistance(point))

    /**
     * Square of the distance between this rectangle and [point]
     * @param point point
     * @return squared distance to [point]
     */
    fun squareDistance(point: PointT) : Double

    /**
     * Returns the i-th corner of this rectangle.
     * The corner indices follow the same order as the quadrants,
     * i.e. the lower left corner in a 2D rectangle has the same index as the lower left quadrant
     * @param index corner index
     * @return i-th corner
     * @see quadrant
     */
    operator fun get(index: Int): PointT

    /**
     * Returns the quadrant in which the point lies in terms of this rectangle.
     * THis is equivalent to the index of the closest corner.
     * @param point point
     * @return quadrant index
     * @see get
     */
    fun quadrant(point: PointT) : Int

    /**
     * @return a reference to this rectangle with the actual type
     */
    fun upcast() : Rect
}

/**
 * An axis-aligned rectangle defined by two points.
 */
class CartesianRectangle<PointT : CartesianPoint<PointT, VectorT>, VectorT : EuclideanVector<VectorT>>(
    point1: PointT,
    point2: PointT
) :
    Rectangle<CartesianRectangle<PointT, VectorT>, PointT, VectorT> {
    val minCorner = point1.minimum(point2)
    val maxCorner = point1.maximum(point2 )
    val center = minCorner + 0.5 * (maxCorner - minCorner)
    val dimensions = minCorner.dimensions()

    override fun squareDistance(point: PointT): Double {
        val minDiff = minCorner - point
        val maxDiff = point - maxCorner
        return minDiff.maximum(maxDiff).maximum(minDiff.empty()).squareLength()
    }

    override operator fun get(index: Int): PointT {
        return minCorner.create((0 until dimensions).map {
            if (index and (1 shl (dimensions - it - 1)) == 0)
                min(minCorner[it], maxCorner[it])
            else
                max(minCorner[it], maxCorner[it])
        }.toList())
    }

    override fun quadrant(point: PointT): Int {
        if (point.isIncomparable) return -1
        var q = 0
        for (i in (0 until dimensions))
            if (point[i] > center[i])
                q += 1 shl (dimensions - i - 1)
        return q
    }

    override fun upcast(): CartesianRectangle<PointT, VectorT> {
        return this
    }

}