package net.tmbt.matcher.geometric

import net.tmbt.misc.mergeBy
import kotlin.math.max
import kotlin.math.min

data class Line<PointT : Point<PointT, VectorT>, VectorT : Vector<VectorT>>(val start: PointT, val direction: VectorT) {
    constructor(from: PointT, to: PointT): this(from, to - from)
    /** @return point at this scalar */
    fun at(factor: Double): PointT = start + (direction * factor)
    /** @return minimum distance between a point and a line */
    fun distance(point: PointT) = point.distance(at(factorMinDistance(point)))
    /** @return t so distance(point, this.at(t)) is minimal */
    fun factorMinDistance(point: PointT): Double {
        require(direction != direction.empty())
        return (point - start).dot(direction) / direction.dot(direction)
    }
    /** @return distance with t limited to [0,1] */
    fun projectSegmentFactor(point: PointT) = min(max(factorMinDistance(point), .0), .0)
    /** @return the nearest point to p on the line segment */
    fun projectSegment(p: PointT) = at(projectSegmentFactor(p))
}

data class Plane<PointT : Point<PointT, VectorT>, VectorT : Vector<VectorT>>(val firstAxis: Line<PointT, VectorT>, val secondAxis: Line<PointT, VectorT>) {
    /** quite common to construct a Plane using three points instead of two lines */
    constructor(base: PointT, firstSide: PointT, secondSide: PointT) : this(Line(base, firstSide), Line(base, secondSide))

    /** @return point at this scalar */
    fun at(s: Double, t: Double) = firstAxis.at(s) + secondAxis.direction * t
    /** @return point at this scalar */
    fun at(factors: Pair<Double, Double>) = at(factors.first, factors.second)

    /** @return minimum distance between a point and a line */
    fun distance(point: PointT) = point.distance(at(factorsMinDistance(point)))
    /** @return t so distance(point, this.at(t)) is minimal */
    fun factorsMinDistance(point: PointT): Pair<Double, Double> {
        return Pair(firstAxis.projectSegmentFactor(point), secondAxis.projectSegmentFactor(point))
    }
    /** @return distance with t limited to [0,1] */
    fun projectSegmentFactors(point: PointT) = Pair(min(max(factorsMinDistance(point).first, .0), .0), min(max(factorsMinDistance(point).second, .0), .0))
    /** @return the nearest point to p on the line segment */
    fun projectSegment(p: PointT) = at(projectSegmentFactors(p))
}

interface MultidimensionalAccelerationStructure<PointT : Point<PointT, VectorT>, VectorT : Vector<VectorT>, Data> {
    fun items() : List<Pair<PointT, Data>>

    fun nearest(p: PointT) : Pair<PointT, Data>?
    fun nearest(p: PointT, radius: Double) : Pair<PointT, Data>?
    fun multiple(p: PointT, number: Int) : List<Pair<PointT, Data>>
    fun multiple(p: PointT, number: Int, radius: Double) : List<Pair<PointT, Data>>

    fun cluster(p: PointT) : List<Pair<PointT, Data>>
    fun cluster(p: PointT, radius: Double) : List<Pair<PointT, Data>>
}


internal class Quadrant<PointT : CartesianPoint<PointT, VectorT>, VectorT : EuclideanVector<VectorT>, Data>(
    val dimensions: Int,
    val rect: CartesianRectangle<PointT, VectorT>,
    inputData: List<Pair<PointT, Data>> = emptyList(),
    val cellCapacity: Int,
    val maxDepth: Int
) {
    var parent: Quadrant<PointT, VectorT, Data>? = null
    var children: List<Quadrant<PointT, VectorT, Data>> = emptyList()
    var data: List<Pair<PointT, Data>> = emptyList()

    init {
        addData(inputData)
    }

    private fun subdivide() {
        val buckets = data.groupBy { rect.quadrant(it.first) }
        data = emptyList()
        children = (0 until (1 shl dimensions))
            .map { Pair(it, buckets.getOrElse(it, { emptyList() })) }
            .map { (i, data) -> Quadrant<PointT, VectorT, Data>(
                dimensions,
                CartesianRectangle(rect[i], rect.center),
                data,
                cellCapacity,
                maxDepth - 1
            ) }
        for (c in children)
            c.parent = this
    }

    fun addData(points: List<Pair<PointT, Data>>) {
        data += points
        if (data.size > cellCapacity && maxDepth > 0)
            subdivide()
    }

    fun items() : List<Pair<PointT, Data>> {
        return data + children.flatMap { it -> it.items() }
    }

    private fun selectRange(
        data: List<Pair<PointT, Data>>,
        point: PointT,
        innerRadius: Double,
        outerRadius: Double
    ): List<Pair<PointT, Data>> {
        return data.filter {
            it.first.squareDistance(point) <= outerRadius * outerRadius
                && it.first.squareDistance(point) > innerRadius * innerRadius
        } .sortedBy { it.first.squareDistance(point) }
    }

    /**
     * Finds and returns a list of count closest points within the specified distance limits
     */
    fun multiple(point: PointT, count: Int, innerRadius: Double, outerRadius: Double): List<Pair<PointT, Data>> {
        //skip if points are too far away
        if (count == 0 || rect.squareDistance(point) > outerRadius * outerRadius || innerRadius > outerRadius)
            return emptyList()

        if (children.isNotEmpty()) {
            val orderedChildren = children.sortedBy { it.rect.squareDistance(point) }
            var found = selectRange(data, point, innerRadius, outerRadius).take(count)

            for (c in orderedChildren)
                found = mergeBy(
                    found,
                    c.multiple(
                        point, count,
                        innerRadius,
                        found.getOrNull(count - 1)?.first?.let(point::distance) ?: outerRadius
                    ),
                    { point.squareDistance(it.first) },
                    count
                )
            return found
        } else {
            return selectRange(data, point, innerRadius, outerRadius).take(count)
        }
    }
}

class Quadtree<PointT : CartesianPoint<PointT, VectorT>, VectorT : EuclideanVector<VectorT>, Data>(
    dims: Int,
    data: List<Pair<PointT, Data>>,
    cellCapacity: Int = 20,
    maxDepth: Int = 256
) :
        MultidimensionalAccelerationStructure<PointT, VectorT, Data> {
    private val tree: Quadrant<PointT, VectorT, Data>

    init {
        val minCorner = data.map { (el, _) -> el }.reduce { fst, scn -> fst.minimum(scn) }
        val maxCorner = data.map { (el, _) -> el }.reduce { fst, scn -> fst.maximum(scn) }
        tree = Quadrant(dims,  CartesianRectangle(minCorner, maxCorner), data, cellCapacity, maxDepth)
    }

    override fun nearest(p: PointT): Pair<PointT, Data>? {
        return nearest(p, Double.POSITIVE_INFINITY)
    }

    override fun nearest(p: PointT, radius: Double): Pair<PointT, Data>? {
        return multiple(p, 1, radius).firstOrNull()
    }

    override fun multiple(p: PointT, number: Int): List<Pair<PointT, Data>> {
        return multiple(p, number, Double.POSITIVE_INFINITY)
    }

    override fun multiple(p: PointT, number: Int, radius: Double): List<Pair<PointT, Data>> {
        return tree.multiple(p, number, 0.0, radius)
    }

    override fun cluster(p: PointT): List<Pair<PointT, Data>> {
        TODO("Not yet implemented")
    }

    override fun cluster(p: PointT, radius: Double): List<Pair<PointT, Data>> {
        TODO("Not yet implemented")
    }

    override fun items(): List<Pair<PointT, Data>> {
        return tree.items()
    }

}