package net.tmbt.matcher.geometric

import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/**
 * an abstract vector class for geometry
 * @param Inherited the class that implements this Vector class
 */
interface Vector<Inherited : Vector<Inherited>> {
    /** make sure that the dimensions actually are matching */
    fun checkDimensions(dims: Int) = dims == dimensions()
    /** @return number of supported dimensions */
    fun dimensions(): Int

    /**
     * compare along an of [0..dimensions()] axis, useful for kd trees
     * @return -1 if smaller, 0 if equal (not same point, just same coordinate) or 1 if greater
     */
    fun compareAlong(other: Vector<Inherited>, axis: Int): Int

    /** @return the length of this vector */
    fun length(): Double = sqrt(squareLength())

    /**
     * @return the squared length of this vector
     */
    fun squareLength(): Double

    /** @return as this is a group, we need a zero vector */
    fun empty(): Inherited
    /** @return new vector = this + other */
    operator fun plus(other: Vector<Inherited>): Inherited
    /** @return new vector = this - other */
    operator fun minus(other: Vector<Inherited>): Inherited
    /** @return new vector = this * scalar */
    operator fun times(scalar: Number): Inherited
    /** @return new vector = this / factor */
    operator fun div(scalar: Number): Inherited
    /** @return the dot product between two vectors */
    fun dot(other: Vector<Inherited>): Double

    /** @return pareto minimum of those two vectors */
    fun minimum(other: Vector<Inherited>): Inherited
    /** @return pareto maximum of those two vectors */
    fun maximum(other: Vector<Inherited>): Inherited

    /**
     * if might be needed to type-safely downcast myself into my implementor
     * @return myself as an instance of Inherited
     */
    fun toInherited(): Inherited
}

/**
 * commutativity is important, therefore 5 * Vector() == Vector() * 5
 * @param Inherited Vector implementor
 * @param vector the Vector object
 */
operator fun <Inherited: Vector<Inherited>> Number.times(vector: Vector<Inherited>): Inherited = vector.times(this)

/**
 * implement commonalities for all vectors \in \mathrm{R}^n \forall n \in \mathrm{N}
 * @param VectorT the vector implementor
 * @param initial the backing data for the constructor
 * @param dims the dimensions of the vector
 */
abstract class EuclideanVector<VectorT: EuclideanVector<VectorT>>(initial: List<Double>, val dims: Int): Vector<VectorT> {
    open val data: List<Double> = initial
    constructor(vararg coords: Double) : this(coords.toList(), coords.size)
    init {
        require(dims == initial.size) {"the size does not match the dimensions of the vector"}
    }

    override fun dimensions(): Int = dims

    override fun compareAlong(other: Vector<VectorT>, axis: Int): Int = compareValues(data[axis], cast(other)[axis])

    override fun squareLength(): Double = data.map { it * it }. sum()

    override fun empty(): VectorT = create(data.map { 0.0 })
    override fun plus(other: Vector<VectorT>): VectorT = create(data.zip(cast(other).data){fst, scn -> fst + scn})
    override fun minus(other: Vector<VectorT>): VectorT = create(data.zip(cast(other).data){fst, scn -> fst - scn})
    override fun times(scalar: Number): VectorT = create(data.map { it * scalar.toDouble() })
    override fun div(scalar: Number): VectorT = create(data.map { it / scalar.toDouble() })
    override fun dot(other: Vector<VectorT>): Double = data.zip(cast(other).data) { fst, scn -> fst * scn }.sum()

    override fun minimum(other: Vector<VectorT>): VectorT = create(data.zip(cast(other).data) { fst, scn -> min(fst, scn) })
    override fun maximum(other: Vector<VectorT>): VectorT = create(data.zip(cast(other).data) { fst, scn -> max(fst, scn) })

    /** actually access data */
    operator fun get(index: Int) = data[index]

    /** we might need to actually create a new object
     * we can not force the existance of a constructor, so we have a method
     */
    abstract fun create(data: List<Double>): VectorT

    private fun cast(v: Vector<VectorT>) = v.toInherited()
}

/**
 * a two-dimensional Vector
 * @param data list of two doubles that contain the point
 */
data class Vector2D(override val data: List<Double>) : EuclideanVector<Vector2D>(data, DIMENSIONS) {
    /**
     * create as empty Vector (identity under addition)
     */
    constructor(): this(Vector2D((0 until DIMENSIONS).map {0.0}).empty().data)

    /**
     * we now know our dimension, so let's use that for convenience
     */
    constructor(fst: Number, scn: Number): this(listOf(fst.toDouble(), scn.toDouble()))

    companion object {
        const val DIMENSIONS = 2
        fun empty() = Vector2D()
    }
    val x
        get() = data[0]
    val y
        get() = data[1]

    override fun create(data: List<Double>) = Vector2D(data)
    override fun toInherited() = this
}

/**
 * a three-dimensional Vector
 * @param data list of two doubles that contain the point
 */
data class Vector3D(override val data: List<Double>) : EuclideanVector<Vector3D>(data, DIMENSIONS) {
    /**
     * create as empty Vector (identity under addition)
     */
    constructor(): this(Vector3D((0 until DIMENSIONS).map {0.0}).empty().data)

    /**
     * we now know our dimension, so let's use that for convenience
     */
    constructor(fst: Number, scn: Number, thrd: Number): this(listOf(fst.toDouble(), scn.toDouble(), thrd.toDouble()))

    companion object {
        const val DIMENSIONS = 3
        fun empty() = Vector3D()
    }
    val x
        get() = data[0]
    val y
        get() = data[1]
    val z
        get() = data[2]

    override fun create(data: List<Double>) = Vector3D(data)
    override fun toInherited() = this
}
