package net.tmbt.matcher

import java.io.File
import java.io.InputStream
import de.topobyte.osm4j.pbf.seq.PbfIterator
import de.topobyte.osm4j.core.model.iface.EntityType
import de.topobyte.osm4j.core.model.iface.EntityContainer
import de.topobyte.osm4j.core.model.iface.OsmNode
import de.topobyte.osm4j.core.model.iface.OsmWay
import de.topobyte.osm4j.core.model.iface.OsmRelation
import de.topobyte.osm4j.core.model.iface.OsmEntity
import de.topobyte.osm4j.core.model.util.OsmModelUtil

fun main(args: Array<String>) {
    println(args.joinToString())
    if (args.size < 2) {
        println("ERROR: too few arguments\n" + "usage: \$COMMAND baseData.pbf gtfsStops.txt")
    }

    val osmDataPath = File(args[0])
    val gtfsDataPath = File(args[1])

    val routes = OSMRouteExtractor(osmDataPath, args.size > 2)
}

class OSMRouteExtractor(val file: File, val print: Boolean = false) {
    var stationNodes = HashSet<OsmNode>()
    var stationNodeOutliers = HashSet<OsmNode>()
    var unnamedStationNodes = HashSet<OsmNode>()
    var nodeCounter = 0L

    val roadWays = HashSet<OsmWay>()
    val railWays = HashSet<OsmWay>()
    val allWays = HashMap<Long, OsmWay>() // pun not intended

    val stationRelations = HashSet<OsmRelation>()
    val unnamedStationRelations = HashSet<OsmRelation>()
    val allRelations = HashMap<Long, OsmRelation>()

    init { parseInitial() }

    val indexedStations = stationNodes.associate {Pair(it.getId(), it)}
    val indexedOutliers = stationNodeOutliers.associate {Pair(it.getId(), it)}
    val indexedUnnamed = unnamedStationNodes.associate {Pair(it.getId(), it)}

    val allRequiredNodes = HashMap<Long, OsmNode>()

    init { reparseCollectMissing() }

    val bundled = HashMap<OsmEntity, List<OsmEntity>>()

    fun parseInitial() {
        val it = PbfIterator(file.inputStream(), false)

        for (element in it) {
            when(element.getType()) {
                EntityType.Node -> parseNodeInitial(element.getEntity() as OsmNode)
                EntityType.Way -> parseWayInitial(element.getEntity() as OsmWay)
                EntityType.Relation -> parseRelationInitial(element.getEntity() as OsmRelation)
                else -> throw IllegalStateException("null should never occur here")
            }
        }
        println("found ${stationNodes.size} station nodes " +
            "[${stationNodeOutliers.size} stationNodeOutliers, overall=${stationNodes.size + stationNodeOutliers.size}; " +
            "${unnamedStationNodes.size} unnamedStationNodes stations]")
        println("found ${roadWays.size} road segments and ${railWays.size} rail segments")
        println("found ${stationRelations.size} station relations "+
            "[${unnamedStationRelations.size} unnamedStationNodes stations]")
        println()
        println("overall: $nodeCounter nodes, ${allWays.size} ways, ${allRelations.size} relations")
    }

    fun reparseCollectMissing() {
        val requiredRelations = HashSet<Long>()
        val requiredNodes = HashSet<Long>()
        val requiredWays = HashSet<Long>()
        val uncheckedRelations = ArrayDeque<Long>()
        for (el in stationRelations)
            uncheckedRelations.add(el.getId())
        while (!uncheckedRelations.isEmpty()) {
            val first = uncheckedRelations.removeFirst()
            if (requiredRelations.contains(first)) continue
            requiredRelations.add(first)
            val rel = allRelations[first]
            for (i in 0 until rel!!.getNumberOfMembers()) {
                val member = rel.getMember(i)
                when(member.getType()) {
                    EntityType.Node -> requiredNodes.add(member.getId())
                    EntityType.Way -> requiredWays.add(member.getId())
                    EntityType.Relation -> {
                        if (member.getId() in requiredRelations) continue
                        requiredRelations.add(member.getId())
                        uncheckedRelations.add(member.getId())
                    }
                    else -> throw IllegalStateException("null should never occur here")
                }
            }
        }
        var nullCounter = 0L
        for (el in roadWays) requiredWays.add(el.getId())
        for (el in railWays) requiredWays.add(el.getId())
        for (id in requiredWays) {
            val way = allWays[id]
            if (way == null) nullCounter++
            else for (i in 0 until way.getNumberOfNodes())
                requiredNodes.add(way.getNodeId(i))
        }
        println("$nullCounter missreferenced ways")

        val it = PbfIterator(file.inputStream(), false)

        for (element in it) {
            if(element.getType() == EntityType.Node && element.getEntity().getId() in requiredNodes)
                allRequiredNodes.put(element.getEntity().getId(), element.getEntity() as OsmNode)
        }
    }

    fun parseNodeInitial(node: OsmNode): Boolean {
        nodeCounter++
        var station = false
        var outlier = false
        for (i in 0 until node.getNumberOfTags()) {
            val tag = node.getTag(i)
            if ((tag.getKey() == "railway" &&
                    tag.getValue() in listOf("halt", "platform", "station", "subway_entrance", "tram_stop"))
                || tag.getKey() == "public_transport" &&
                    tag.getValue() in listOf("stop_position", "platform", "station")) {
                station = true
                continue
            } else if (tag.getKey() in listOf("railway", "public_transport") &&
                tag.getValue() in listOf("halt", "platform", "station",
                    "subway_entrance", "tram_stop", "stop_position")) {
                outlier = true
            }
        }
        if (!station && !outlier) return false

        if (station) stationNodes.add(node)
        if (outlier) stationNodeOutliers.add(node)

        var map = OsmModelUtil.getTagsAsMap(node)
        var name = map["name"]
        if (name == null) {
            unnamedStationNodes.add(node)
            if(print) {
                System.err.println("checking UNNAMED ${node.getLatitude()} ${node.getLongitude()}: ---------")
                for ((key, value) in map) {
                    System.err.println("o: $name: $key=$value")
                }
            }
        } else if (print) {
            if (outlier) {
                println("outlier: $name, id=${node.getId()}")
                System.err.println("checking OUTLIER $name: ---------")
                for ((key, value) in map) {
                    System.err.println("o: $name: $key=$value")
                }
            }
            else println("station: $name, id=${node.getId()}")
        }
        return true
    }

    fun parseWayInitial(way: OsmWay): Boolean {
        val map = OsmModelUtil.getTagsAsMap(way)
        var found = false
        if ("highway" in map) {
            found = true
            roadWays.add(way)
            if (print) System.err.println("found road segment \"${map["name"]}\" with " +
                "${way.getNumberOfNodes()} nodes")
        }
        if ("railway" in map) {
            found = true
            railWays.add(way)
            if (print) System.err.println("found rail segment \"${map["name"]}\" with " +
                "${way.getNumberOfNodes()} nodes")
        }
        allWays.put(way.getId(), way)
        return found
    }

    fun parseRelationInitial(rel: OsmRelation): Boolean {
        allRelations.put(rel.getId(), rel)
        val map = OsmModelUtil.getTagsAsMap(rel)
        if (map["public_transport"] == "stop_area") {
            stationRelations.add(rel)
            val name = map["name"]

            stationRelations.add(rel)
            if (name == null) unnamedStationRelations.add(rel)

            if (print) {
                val nodeCount = OsmModelUtil.membersAsList(rel).stream().filter {it.getType() == EntityType.Node}.count()
                val wayCount = OsmModelUtil.membersAsList(rel).stream().filter {it.getType() == EntityType.Way}.count()
                System.err.println("station relation $name with $nodeCount nodes and $wayCount ways")
            }
            return true
        } else return false
    }
}
