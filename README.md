# Matcher

We get a lot of faulty data:
1. Data from the Overpass API which mostly contains stations and track information, but with incomplete time table information
2. Data from the various time table GTFS files, that is very badly geotagged, e.g. contains no relational information.
3. Data from real time APIs that is even worse

PS: This is a commit